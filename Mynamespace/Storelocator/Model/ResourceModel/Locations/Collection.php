<?php

namespace Mynamespace\Storelocator\Model\ResourceModel\Locations;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends Abstractcollection
{
    protected $_idFieldName = \Mynamespace\Storelocator\Model\Locations::STORELOCATOR_ID;

    /**
     * Define model & resource model
     */
    protected function _construct()
    {
        $this->_init(
            'Mynamespace\Storelocator\Model\Locations',
            'Mynamespace\Storelocator\Model\ResourceModel\Locations'
        );
    }
}