<?php

namespace Mynamespace\Storelocator\Model;

use Magento\Framework\Model\AbstractModel;

class Locations extends AbstractModel
{
    const STORELOCATOR_ID = 'id'; // We define the id fieldname

    /**
     * Name of object id field
     *
     * @var string
     */
    protected $_idFieldName = self::STORELOCATOR_ID;

    /**
     * Define resource model
     */
    protected function _construct()
    {
        $this->_init('Mynamespace\Storelocator\Model\ResourceModel\Locations');
    }
}