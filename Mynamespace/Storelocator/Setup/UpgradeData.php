<?php

namespace Mynamespace\Storelocator\Setup;

use Magento\Framework\Setup\UpgradeDataInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;

/**
 * This file will check the module version and insert some sample data into the custom table
 */
class UpgradeData implements UpgradeDataInterface
{
    public function upgrade(
        ModuleDataSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $setup->startSetup();

        if (version_compare($context->getVersion(), '0.0.4') < 0) {
            /**
             * Get mynamespace_storelocator table
             */
            $tableName = $setup->getTable('mynamespace_storelocator');
            /**
             * Check if the table already exists
             */
            if ($setup->getConnection()->isTableExists($tableName) == true) {
                /**
                 * Declare data
                 */
                $data = [
                    [
                        'store_name' => 'Storename',
                        'country' => 'country',
                        'state' => 'state',
                        'city' => 'city',
                        'address' => 'address',
                        'postcode' => '560068',
                        'created_at' => date('Y-m-d H:i:s')
                    ]
                ];

                /**
                 *  Insert data to table
                 */
                foreach ($data as $item) {
                    $setup->getConnection()->insert($tableName, $item);
                }
            }
        }

        $setup->endSetup();
    }
}