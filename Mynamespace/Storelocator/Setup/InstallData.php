<?php

namespace Mynamespace\Storelocator\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;

/**
 * This file will insert the sample data into the custom database table when your module is installed
 */
class InstallData implements InstallDataInterface
{
    public function install(
        ModuleDataSetupInterface $setup,
        ModuleContextInterface $context
    ) {
        $setup->startSetup();

        // Get mynamespace_storelocator table
        $tableName = $setup->getTable('mynamespace_storelocator');
        // Check if the table already exists
        if ($setup->getConnection()->isTableExists($tableName) == true) {
            // Declare data
            $data = [
                [
                    'store_name' => 'Store1',
                    'country' => 'country1',
                    'state' => 'state1',
                    'city' => 'city1',
                    'address' => 'address1',
                    'postcode' => '560068',
                    'created_at' => date('Y-m-d H:i:s')
                ],
                [
                    'store_name' => 'Store2',
                    'country' => 'country2',
                    'state' => 'state2',
                    'city' => 'city2',
                    'address' => 'address2',
                    'postcode' => '560068',
                    'created_at' => date('Y-m-d H:i:s')
                ]
            ];

            // Insert data to table
            foreach ($data as $item) {
                $setup->getConnection()->insert($tableName, $item);
            }
        }

        $setup->endSetup();
    }
}