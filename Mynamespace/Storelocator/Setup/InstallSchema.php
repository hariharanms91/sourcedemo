<?php

namespace Mynamespace\Storelocator\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

/**
 * This file will declare and create your custom table
 */
class InstallSchema implements InstallSchemaInterface
{
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        /**
         * Get mynamespace_storelocator table
         */
        $tableName = $installer->getTable('mynamespace_storelocator');
        
        /**
         * check if the table already exists
         */
        if($installer->getConnection()->isTableExists($tableName) != true)
        {
            /**
             * create mynamespace_storelocator table
             */
            $table = $installer->getConnection()
                ->newTable($tableName)
                ->addColumn(
                    'id',
                    Table::TYPE_INTEGER,
                    null,
                    [
                        'identity' => true,
                        'unsigned' => true,
                        'nullable' => false,
                        'primary' => true
                    ],
                    'ID'
                )
                ->addColumn(
                    'store_name',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => false,'default' => ''],
                    'Store Name'
                )
                ->addColumn(
                    'country',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => false,'default' => ''],
                    'Country'
                )
                ->addColumn(
                    'state',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => false,'default' => ''],
                    'State'
                )
                ->addColumn(
                    'city',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => false,'default' => ''],
                    'City'
                )
                ->addColumn(
                    'address',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => false,'default' => ''],
                    'Address'
                )
                ->addColumn(
                    'postcode',
                    Table::TYPE_TEXT,
                    255,
                    ['nullable' => true, 'default' => null],
                    'Zip/Postal Code'
                )
                ->addColumn(
                    'latitude',
                    Table::TYPE_FLOAT,
                    255,
                    ['nullable' => true, 'default' => null],
                    'Latitude'
                )
                ->addColumn(
                    'longitude',
                    Table::TYPE_FLOAT,
                    255,
                    ['nullable' => true, 'default' => null],
                    'Longitude'
                )
                ->addColumn(
                    'created_at',
                    Table::TYPE_TIMESTAMP,
                    null,
                    ['nullable' => false, 'default' => Table::TIMESTAMP_INIT],
                    'Created At'
                )->addColumn(
                    'updated_at',
                    Table::TYPE_TIMESTAMP,
                    null,
                    ['nullable' => false, 'default' => Table::TIMESTAMP_INIT_UPDATE],
                    'Updated At'
                )
                ->setComment('Storelocator Table')
                ->setOption('type', 'InnoDB')
                ->setOption('charset', 'utf8');
            $installer->getConnection()->createTable($table);
        }

        $installer->endSetup();
    }
}