<?php
namespace Mynamespace\Storelocator\Setup;

use Magento\Framework\Setup\UpgradeSchemaInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;

class UpgradeSchema implements UpgradeSchemaInterface
{
    public function upgrade(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();
        if(version_compare($context->getVersion(),'0.0.8','<')){
            $installer->getConnection()->addColumn(
                $installer->getTable('mynamespace_storelocator'),
                'status',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
                    'nullable' => true,
                    'default' => '0',
                    'after' => 'postcode',
                    'comment' => 'Status'
                ]
            );
        }

        if(version_compare($context->getVersion(),'0.0.14','<')){
            /**
             * Add full text index to our table storelocator
             */

             $tableName = $installer->getTable('mynamespace_storelocator');
             $fullTextIndex = array('store_name'); // Column with fulltext index

             $installer->getConnection()->addIndex(
                $tableName,
                $installer->getIdxName($tableName, $fullTextIndex, \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_FULLTEXT),
                $fullTextIndex,
                \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_FULLTEXT
             );
        }

        if(version_compare($context->getVersion(),'0.0.15','<')){
            $installer->getConnection()->addColumn(
                $installer->getTable('mynamespace_storelocator'),
                'state_id',
                [
                    'type' => \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    'nullable' => true,
                    'default' => null,
                    'after' => 'state',
                    'comment' => 'State Id'
                ]
            );
        }

    }
}