<?php
namespace Mynamespace\Storelocator\Block;

use Magento\Framework\View\Element\Template;
use \Mynamespace\Storelocator\Model\ResourceModel\Locations\CollectionFactory;
use \Psr\Log\LoggerInterface;

class Locations extends Template
{
    /**
     * @var \Mynamespace\Storelocator\Model\CollectionFactory
     */
    protected $_locationsCollectionFactory;

    /**
     *  @var \Psr\Log\LoggerInterface
     */
    protected $_logger;

    /**
     * @param Template\Context $context
     * @param CollectionFactory $locationsCollectionFactory
     * @param LoggerInterface $logger
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        CollectionFactory $locationsCollectionFactory,
        LoggerInterface $logger,
        array $data = []
    )
    {
        parent::__construct($context, $data);
        $this->_locationsCollectionFactory = $locationsCollectionFactory;
        $this->_logger = $logger;
    }

    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        $this->pageConfig->getTitle()->set(__('Store Locator'));

        if ($this->getLocationsHistory()) {
            $pager = $this->getLayout()->createBlock(
                'Magento\Theme\Block\Html\Pager',
                'storelocator.history.pager'
            )->setAvailableLimit(array(5=>5,10=>10,15=>15,20=>20))
                ->setShowPerPage(true)->setCollection(
                $this->getLocationsHistory()
            );
            $this->setChild('pager', $pager);
            $this->getLocationsHistory()->load();
        }
        return $this;
    }

    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }
    /**
     * function to get Store locations
     *
     * @return storelocator collection
     */
    Public function getLocationsHistory()
    {
        //get values of current page
        $page=($this->getRequest()->getParam('p'))? $this->getRequest()->getParam('p') : 1;
        //get values of current limit
        $pageSize=($this->getRequest()->getParam('limit'))? $this->getRequest
        ()->getParam('limit') : 5;


        $collection = $this->getCollection();
        $collection->setPageSize($pageSize);
        $collection->setCurPage($page);
        $this->_logger->info("Here Storelocator : ".$collection->getSelect());
        $this->_logger->info("Here Storelocator : Page:".$page." Page size :"
            .$pageSize);
        return $collection;
    }

    public function getCollection(){
        $collection = $this->_locationsCollectionFactory->create();
        return $collection;     
    }
}