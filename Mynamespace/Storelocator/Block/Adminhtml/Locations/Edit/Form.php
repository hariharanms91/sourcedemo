<?php
namespace Mynamespace\Storelocator\Block\Adminhtml\Locations\Edit;
 
use \Magento\Backend\Block\Widget\Form\Generic;
 
class Form extends Generic
{
    /**
     * @var \Magento\Directory\Model\Config\Source\Country
     */
    protected $_country;

    /**
     * @var \Magento\Directory\Model\RegionFactory
     */
    protected $_regionFactory;
 
    /**
     * @var \Magento\Store\Model\System\Store
     */
    protected $_systemStore;
 
    /**
     * @param \Magento\Backend\Block\Template\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Data\FormFactory $formFactory
     * @param \Magento\Store\Model\System\Store $systemStore
     * @param \Magento\Directory\Model\Config\Source\Country $country
     * @param \Magento\Directory\Model\RegionFactory $regionFactory
     * @param array $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Framework\Data\FormFactory $formFactory,
        \Magento\Store\Model\System\Store $systemStore,
        \Magento\Directory\Model\Config\Source\Country $country,
        \Magento\Directory\Model\RegionFactory $regionFactory,
        array $data = []
    ) {
        parent::__construct($context, $registry, $formFactory, $data);
        $this->_systemStore = $systemStore;
        $this->_country = $country;
        $this->_regionFactory = $regionFactory;

    }
 
    /**
     * Init form
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        $this->setId('storelocator_form');
        $this->setTitle(__('Storelocator Information'));
    }
 
    /**
     * Prepare form
     *
     * @return $this
     */
    protected function _prepareForm()
    {
        
        /** @var \Mynamespace\Storelocator\Model\Locations $model */
        $model = $this->_coreRegistry->registry('storelocator_locations');
        /** @var \Magento\Framework\Data\Form $form */
        $form = $this->_formFactory->create(
            ['data' => ['id' => 'edit_form', 'action' => $this->getData('action'), 'method' => 'post']]
        );
       
        $form->setHtmlIdPrefix('storelocator_');
 
        $fieldset = $form->addFieldset(
            'base_fieldset',
            ['legend' => __('General Information'), 'class' => 'fieldset-wide']
        );
 
        if ($model->getId()) {
            $fieldset->addField('id', 'hidden', ['name' => 'id']);
        }
 
        $fieldset->addField(
            'store_name',
            'text',
            ['name' => 'store_name', 'label' => __('Store Name'), 'title' => __('Store Name'), 'required' => true]
        );

        $countries = $this->_country->toOptionArray();
         
        $regionCollection = $this->_regionFactory->create()->getCollection()->addCountryFilter($model->getCountryId());
        $regions = $regionCollection->toOptionArray();
        
        $fieldset->addField(
            'country',
            'select',
            ['name' => 'country', 'label' => __('Country'), 'required' => true, 'values' => $countries]
        );

        $fieldset->addField(
            'state_id',
            'select',
            ['name' => 'state_id', 'label' => __('State'), 'values' => $regions]
        );

        $fieldset->addField(
            'state',
            'text',
            ['name' => 'state', 'label' => __('State')]
        );

        $fieldset->addField(
            'city',
            'text',
            ['name' => 'city', 'label' => __('City')]
        );
        $fieldset->addField(
            'address',
            'text',
            ['name' => 'address', 'label' => __('Address')]
        );

        $fieldset->addField(
            'postcode',
            'text',
            ['name' => 'postcode', 'label' => __('Postcode')]
        );

        $this->setChild(
            'form_after',
            $this->getLayout()->createBlock('Magento\Framework\View\Element\Template')->setTemplate('Mynamespace_Storelocator::js.phtml')
        );
 
        $form->setValues($model->getData());
        $form->setUseContainer(true);
        $this->setForm($form);
 
        return parent::_prepareForm();
    }
}