<?php
namespace Mynamespace\Storelocator\Controller\Adminhtml\Locations;

use Magento\Backend\App\Action;

class NewAction extends Action
{
    /**
     *  @var \Magento\Backend\Model\View\Result\ForwardFactory
     */
    protected $_resultForwardFactory;

    /**
     *  @param \Magento\Backend\App\Action\Context $context
     *  @param \param \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Backend\Model\View\Result\ForwardFactory $resultForwardFactory
    )
    {
        parent::__construct($context);
        $this->_resultForwardFactory = $resultForwardFactory;
    }

    /**
     *  {@inheritDoc}
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed('Mynamespace_Storelocator::locations_save');
    }

    /**
     *  Forward to edit
     * 
     *  @return \Magento\Backend\Model\View\Result\Forward
     */
    public function execute()
    { 
        /** @var \Magento\Backend\Model\View\Result\Forward $resultForwardFactory */
        return $this->_resultForwardFactory->create()->forward('edit');
    }

}