<?php
namespace Mynamespace\Storelocator\Controller\Adminhtml\Locations;

use Magento\Backend\App\Action;

class Edit extends Action
{
    /**
     *  Core registry
     * 
     *  @var \Magento\Framework\Registry
     */
    protected $_coreRegistry = null;

    /**
     *  @var \Magento\Framework\View\Result\PageFactory
     */
    protected $_resultPageFactory;

    /**
     *  @var \Mynamespace\Storelocator\Model\Locations
     */

     protected $_model;

     /**
      * @param Action\Context $context
      * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory,
      * @param \Magento\Framework\Registry $registry
      * @param \Mynamespace\Storelocator\Model\Locations $model
      */
      public function __construct(
          Action\Context $context,
          \Magento\Framework\View\Result\PageFactory $resultPageFactory,
          \Magento\Framework\Registry $registry,
          \Mynamespace\Storelocator\Model\Locations $model
      )
      {
          parent::__construct($context);
          $this->_resultPageFactory = $resultPageFactory;
          $this->_coreRegistry = $registry;
          $this->_model = $model;
      }

      /**
       *  Init actions
       * 
       *  @return \Magento\Backend\Model\View\Result\Page
       */
      protected function _initAction()
      {
          // load layout, set active menu and breadcrumbs
          /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
          $resultPage = $this->_resultPageFactory->create();
          $resultPage->setActiveMenu('Mynamespace_Storelocator::locations')
            ->addBreadcrumb(__('Locations'),__('Locations'))
            ->addBreadcrumb(__('Manage Locations'),__('Manage Locations'));
          return $resultPage;
      }

      /**
       * Edit Location
       * 
       * @return \Magento\Backend\Model\View\Result\Page|\Magento\Backend\Model\View\Result\Redirect
       * @SuppressWarnings(PHPMD.NPathComplexity)
       */
      public function execute()
      { 
            $id = $this->getRequest()->getParam('id');
            $model = $this->_model;

            // If you have got an id, it's edition
            if($id){
                $model->load($id);
                if(!$model->getId()) {
                    $this->messageManager->addError(__('This location not exists.'));
                    /** \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
                    $resultRedirect = $this->resultRedirectFactory->create();

                    return $resultRedirect->setPath('*/*/');
                }
            }

            $data = $this->_getSession()->getFormData(true);
            if(!empty($data)){
                $model->setData($data);
            }

            $this->_coreRegistry->register('storelocator_locations', $model);

            /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
            $resultPage = $this->_initAction();
            $resultPage->addBreadcrumb(
                $id ? __('Edit Location') : __('New Location'),
                $id ? __('Edit Location') : __('New Location')
            );
            $resultPage->getConfig()->getTitle()->prepend(__('Locations'));
            $resultPage->getConfig()->getTitle()
                ->prepend($model->getId() ? $model->getStoreName() : __('New Location'));
    
            return $resultPage;
      }

}