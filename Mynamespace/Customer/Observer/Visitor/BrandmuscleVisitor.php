<?php
namespace Mynamespace\Customer\Observer\Visitor;

use \Magento\Framework\Event\Observer;
use \Magento\Framework\Event\ObserverInterface;
use Magento\Customer\Model\Url as customerConfig;
use Magento\Framework\HTTP\Client\Curl;
use Magento\Framework\App\Response\Http as Redirect;


use Magento\Wishlist\Controller\WishlistProviderInterface;

class MynamespaceVisitor implements ObserverInterface
{
    protected $customerSession;
    protected $customerConfig;
    protected $curl;
    protected $redirect;


    protected $wishlistProvider;


    public function __construct(
        \Magento\Customer\Model\Session $customerSession,
        customerConfig $customerConfig,
        Curl $curl,
        Redirect $redirect,
        

        WishlistProviderInterface $wishlistProvider)
    {
        $this->customerSession = $customerSession;
        $this->customerConfig = $customerConfig;
        $this->curl = $curl;
        $this->redirect = $redirect;


        $this->wishlistProvider = $wishlistProvider;

    }

    public function execute(Observer $observer)
    { //echo 'adfasdf'; exit;

        /*$currentUserWishlist = $this->wishlistProvider->getWishlist();
        if ($currentUserWishlist) {
            $wishlistItems = $currentUserWishlist->getItemCollection();
        }
        foreach($wishlistItems as $item)
        {
           print_r($item->getDescription()); exit;
        }
        echo '<pre>'; print_r($wishlistItems); echo '</pre>'; exit;*/


        $bmHomeUrl = $this->customerConfig->getBmHomeUrl();
        if ($this->customerSession->isLoggedIn()) { 
            if($token = $this->customerSession->getToken())
            {
                $bmServiceUrl = $this->customerConfig->getBmServiceUrl();
                $this->curl->post($bmServiceUrl, ['token'=>$token]);

                if($this->curl->getStatus() !== 200)
                {
                    $this->customerSession->unsToken();
                    $this->customerSession->logout();
                    $this->redirect->setRedirect($bmHomeUrl);
                }
            }
        }
        else
        {
            //$this->redirect->setRedirect($bmHomeUrl);
        }
    }
}