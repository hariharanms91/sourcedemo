<?php
namespace Mynamespace\Customer\Controller\Account;

use Magento\Customer\Model\Account\Redirect as AccountRedirect;
use Magento\Framework\App\Action\Context;
use Magento\Customer\Model\Session;
use Magento\Customer\Api\AccountManagementInterface;
use Magento\Customer\Model\Url as CustomerUrl;
use Magento\Framework\Exception\EmailNotConfirmedException;
use Magento\Framework\Exception\AuthenticationException;
use Magento\Framework\Data\Form\FormKey\Validator;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\State\UserLockedException;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\HTTP\Client\Curl;
use Magento\Customer\Model\Customer;
use Magento\Framework\Json\Helper\Data as jsonHelper;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Customer\Model\CustomerFactory;

class LoginPost extends \Magento\Customer\Controller\Account\LoginPost
{
    /**
     * @var \Magento\Customer\Api\AccountManagementInterface
     */
    protected $customerAccountManagement;

    /**
     * @var \Magento\Framework\Data\Form\FormKey\Validator
     */
    protected $formKeyValidator;

    /**
     * @var AccountRedirect
     */
    protected $accountRedirect;

    /**
     * @var Session
     */
    protected $session;

    /**
     * @var ScopeConfigInterface
     */
    private $scopeConfig;

    /**
     * @var \Magento\Framework\Stdlib\Cookie\CookieMetadataFactory
     */
    private $cookieMetadataFactory;

    /**
     * @var \Magento\Framework\Stdlib\Cookie\PhpCookieManager
     */
    private $cookieMetadataManager;

    /**
     * @var Curl
     */
    private $curl;

    /**
     * @var Customer
     */
    private $customer;

    /**
     * @var jsonHelper
     */
    private $jsonHelper;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * @var CustomerFactory
     */
    private $customerFactory;

    /**
     * @param Context $context
     * @param Session $customerSession
     * @param AccountManagementInterface $customerAccountManagement
     * @param CustomerUrl $customerHelperData
     * @param Validator $formKeyValidator
     * @param AccountRedirect $accountRedirect
     */
    public function __construct(
        Context $context,
        Session $customerSession,
        AccountManagementInterface $customerAccountManagement,
        CustomerUrl $customerHelperData,
        Validator $formKeyValidator,
        AccountRedirect $accountRedirect,
        Curl $curl,
        Customer $customer,
        jsonHelper $jsonHelper,
        StoreManagerInterface $storeManager,
        CustomerFactory $customerFactory

    ) {
        $this->customerSession = $customerSession;
        $this->customerAccountManagement = $customerAccountManagement;
        $this->customerUrl = $customerHelperData;
        $this->formKeyValidator = $formKeyValidator;
        $this->accountRedirect = $accountRedirect;
        $this->curl = $curl;
        $this->customer = $customer;
        $this->jsonHelper = $jsonHelper;
        $this->storeManager = $storeManager;
        $this->customerFactory  = $customerFactory;
        parent::__construct(
            $context,
            $customerSession,
            $customerAccountManagement,
            $customerHelperData,
            $formKeyValidator,
            $accountRedirect

        );
    }

    public function execute()
    { 
        if ($this->getRequest()->isGet()) {
            $serviceUrl =  $this->customerUrl->getBmServiceUrl();
            if($token = $this->getRequest()->getParam('token')) {

                $this->curl->post($serviceUrl, ['token'=>$token]);
                $jsonData = $this->curl->getBody();
               
                $response = $this->jsonHelper->jsonDecode($jsonData);
                $response = $response['data'];
                $email = $response['email'];
                $websiteId = 1;

                $currentcustomer = $this->customer->setWebsiteId($websiteId)->loadByEmail($email);
                if(!$currentcustomer->getId()) {
                    // Get Website ID
                    $websiteId  = $this->storeManager->getWebsite()->getWebsiteId();

                    // Instantiate object (this is the most important part)
                    $currentcustomer   = $this->customerFactory->create();
                    $currentcustomer->setWebsiteId($websiteId);

                    // Preparing data for new customer
                    $currentcustomer->setEmail($email); 
                    $currentcustomer->setFirstname($email);
                    $currentcustomer->setLastname($email);

                    // Save data
                    $currentcustomer->save();

                   
                }
                $this->customerSession->setToken($token);
                $this->customerSession->setCustomerAsLoggedIn($currentcustomer);
                return $this->accountRedirect->getRedirect();
                 
                 
            }
            
        }
        return parent::execute();
    }
}