<?php
namespace Mynamespace\Customer\Model;

class Url extends \Magento\Customer\Model\Url
{
    const BM_V5_SERVICEURL = 'bmauth/bmauthGroup/bmV5authservieurl';

    const BM_V5_HOMEURL = 'bmauth/bmauthGroup/bmV5homeurl';


    public function getBmServiceUrl()
    {
        return $this->scopeConfig->getValue(
            self::BM_V5_SERVICEURL,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }

    public function getBmHomeUrl()
    {
        return $this->scopeConfig->getValue(
            self::BM_V5_HOMEURL,
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }
}