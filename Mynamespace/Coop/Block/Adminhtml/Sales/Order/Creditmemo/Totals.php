<?php

namespace Mynamespace\Coop\Block\Adminhtml\Sales\Order\Creditmemo;

class Totals extends \Magento\Framework\View\Element\Template
{
    /**
     * Order invoice
     *
     * @var \Magento\Sales\Model\Order\Creditmemo|null
     */
    protected $_creditmemo = null;

    /**
     * @var \Magento\Framework\DataObject
     */
    protected $_source;

    /**
     * @var \Mynamespace\Coop\Helper\Data
     */
    protected $_dataHelper;

    /**
     * OrderCoopcredit constructor.
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
         \Mynamespace\Coop\Helper\Data $dataHelper,
        array $data = []
    ) {
        $this->_dataHelper = $dataHelper;
        parent::__construct($context, $data);
    }

    /**
     * Get data (totals) source model
     *
     * @return \Magento\Framework\DataObject
     */
    public function getSource()
    {
        return $this->getParentBlock()->getSource();
    }

    public function getCreditmemo()
    {
        return $this->getParentBlock()->getCreditmemo();
    }
    /**
     * Initialize payment coopcredit totals
     *
     * @return $this
     */
    public function initTotals()
    {
        $this->getParentBlock();
        $this->getCreditmemo();
        $this->getSource();

        if(!$this->getSource()->getCoopcredit()) {
            return $this;
        }
        $coopcredit = new \Magento\Framework\DataObject(
            [
                'code' => 'coopcredit',
                'strong' => false,
                'value' => '-'.$this->getSource()->getCoopcredit(),
                'label' => $this->_dataHelper->getCoopcreditLabel(),
            ]
        );

        $this->getParentBlock()->addTotalBefore($coopcredit, 'grand_total');

        return $this;
    }
}
