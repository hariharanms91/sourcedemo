<?php

namespace Mynamespace\Coop\Model\Invoice\Total;

use Magento\Sales\Model\Order\Invoice\Total\AbstractTotal;

class Coopcredit extends AbstractTotal
{
    /**
     * @param \Magento\Sales\Model\Order\Invoice $invoice
     * @return $this
     */
    public function collect(\Magento\Sales\Model\Order\Invoice $invoice)
    {
        $invoice->setCoopcredit(0);
        
        $coopcredit = $invoice->getOrder()->getCoopcredit();
        $invoice->setCoopcredit($coopcredit);
        $invoice->setGrandTotal($invoice->getGrandTotal() - $coopcredit);
        

        return $this;
    }
}
