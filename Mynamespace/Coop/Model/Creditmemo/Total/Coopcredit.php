<?php

namespace Mynamespace\Coop\Model\Creditmemo\Total;

use Magento\Sales\Model\Order\Creditmemo\Total\AbstractTotal;

class Coopcredit extends AbstractTotal
{
    /**
     * @param \Magento\Sales\Model\Order\Creditmemo $creditmemo
     * @return $this
     */
    public function collect(\Magento\Sales\Model\Order\Creditmemo $creditmemo)
    {
        $creditmemo->setCoopcredit(0);
        
        $coopcredit = $creditmemo->getOrder()->getCoopcredit();
        $creditmemo->setCoopcredit($coopcredit);

        $creditmemo->setGrandTotal($creditmemo->getGrandTotal() - $coopcredit);

        return $this;
    }
}
