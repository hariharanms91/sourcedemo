<?php
namespace Mynamespace\Coop\Model;

use Magento\Checkout\Model\ConfigProviderInterface;

class CoopConfigProvider implements ConfigProviderInterface
{
    /**
     * @var \Mynamespace\Coop\Helper\Data
     */
    protected $dataHelper;

    /**
     * @var \Magento\Checkout\Model\Session
     */
    protected $checkoutSession;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $logger;

    /**
     * @param \Mynamespace\Coop\Helper\Data $dataHelper
     * @param \Magento\Checkout\Model\Session $checkoutSession
     * @param \Psr\Log\LoggerInterface $logger
     */
    public function __construct(
        \Mynamespace\Coop\Helper\Data $dataHelper,
        \Magento\Checkout\Model\Session $checkoutSession,
        \Psr\Log\LoggerInterface $logger

    )
    {
        $this->dataHelper = $dataHelper;
        $this->checkoutSession = $checkoutSession;
        $this->logger = $logger;
    }

    /**
     * @return array
     */
    public function getConfig()
    {
        $CoopConfig = [];
        $enabled = $this->dataHelper->isModuleEnabled();
        $CoopConfig['coopcredit_label'] = $this->dataHelper->getCoopcreditLabel();
        $quote = $this->checkoutSession->getQuote();
        $subtotal = $quote->getSubtotal();
        $coopcredit = $this->dataHelper->getCoop();
        //$coopcredit = ($subtotal/100) * $coopcredit;
        $CoopConfig['coop_credit_amount'] = $quote->getCoopcredit(); //$quote.getCoopcredit();
        $CoopConfig['show_hide_Coop_block'] = ($enabled && $coopcredit) ? true : false;
        $CoopConfig['show_hide_Coop_shipblock'] = ($enabled) ? true : false;
        return $CoopConfig;
    }
}
