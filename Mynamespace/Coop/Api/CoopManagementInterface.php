<?php


namespace Mynamespace\Coop\Api;

/**
 * Coop management service interface.
 * @api
 */
interface CoopManagementInterface
{
    /**
     * Returns information for a coopcredit in a specified cart.
     *
     * @param int $cartId The cart ID.
     * @return string The coopcredit code data.
     * @throws \Magento\Framework\Exception\NoSuchEntityException The specified cart does not exist.
     */
    public function get($cartId);

    /**
     * Adds CoopCredit to a specified cart.
     *
     * @param int $cartId The cart ID.
     * @param string $coopCredit The coopcredit code data.
     * @return bool
     * @throws \Magento\Framework\Exception\NoSuchEntityException The specified cart does not exist.
     * @throws \Magento\Framework\Exception\CouldNotSaveException The specified coopcredit could not be added.
     */
    public function set($cartId, $coopCredit);

    /**
     * Deletes CoopCredit from a specified cart.
     *
     * @param int $cartId The cart ID.
     * @return bool
     * @throws \Magento\Framework\Exception\NoSuchEntityException The specified cart does not exist.
     * @throws \Magento\Framework\Exception\CouldNotDeleteException The specified coopcredit could not be deleted.
     */
    //public function remove($cartId);
}
