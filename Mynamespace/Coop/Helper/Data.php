<?php

namespace Mynamespace\Coop\Helper;

use Magento\Framework\App\Helper\AbstractHelper;

class Data extends AbstractHelper
{
    /**
     * Custom coopcredit config path
     */
    const CONFIG_CUSTOM_IS_ENABLED = 'Coop/Coop/status';
    const CONFIG_COOPCREDIT = 'Coop/Coop/Coop_amount';
    const CONFIG_COOPCREDIT_LABEL = 'Coop/Coop/name';
    const CONFIG_MINIMUM_ORDER_AMOUNT = 'Coop/Coop/minimum_order_amount';

    /**
     * @return mixed
     */
    public function isModuleEnabled()
    {

        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
        $isEnabled = $this->scopeConfig->getValue(self::CONFIG_CUSTOM_IS_ENABLED, $storeScope);
        return $isEnabled;
    }

    /**
     * Get custom coopcredit
     *
     * @return mixed
     */
    public function getCoop()
    {
        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
        $coopcredit = $this->scopeConfig->getValue(self::CONFIG_COOPCREDIT, $storeScope);
        return $coopcredit;
    }

    /**
     * Get custom coopcredit
     *
     * @return mixed
     */
    public function getCoopcreditLabel()
    {
        $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
        $coopcreditLabel = $this->scopeConfig->getValue(self::CONFIG_COOPCREDIT_LABEL, $storeScope);
        return $coopcreditLabel;
    }
}
