define([
    'ko',
    'uiComponent',
    'Magento_Checkout/js/model/quote',
    'Magento_Catalog/js/price-utils',
    'Mynamespace_Coop/js/action/set-coop-credit',
    'Magento_Checkout/js/model/totals'

], function (ko, Component,quote,priceUtils,setCoopCreditAction,totals) {
    'use strict';
    var show_hide_Coop_blockConfig = window.checkoutConfig.show_hide_Coop_block;
    var coopcredit_label = window.checkoutConfig.coopcredit_label;
    var coop_credit_amount = 1000;

    return Component.extend({
        getCoopcreditLabel:ko.observable(coopcredit_label),
        getCoopcreditAmount:ko.observable(priceUtils.formatPrice(coop_credit_amount, quote.getPriceFormat())),
        coopcredituse: ko.observable(totals.getSegment('coopcredit').value),
        isDisplayed: function () {
            return this.getValue() != 0;
        },

        getValue: function() {
            return coop_credit_amount;
        },
        isSelectCoopCredit: ko.observable(false),
        coopAppliedAmountDisplay: function()
        {
            return priceUtils.formatPrice(totals.getSegment('coopcredit').value, quote.getPriceFormat());
        },
        
        applyCoop: function(){
            
            setCoopCreditAction(this.coopcredituse());
            return false;
        }
    });
});
