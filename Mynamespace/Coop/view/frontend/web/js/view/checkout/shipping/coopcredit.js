
define([
        'ko',
        'uiComponent',
        'Magento_Checkout/js/model/quote',
        'Magento_Catalog/js/price-utils'

    ], function (ko, Component, quote, priceUtils) {
        'use strict';
        var show_hide_Coop_blockConfig = window.checkoutConfig.show_hide_Coop_shipblock;
        var coopcredit_label = window.checkoutConfig.coopcredit_label;         
        var coop_credit_amount = window.checkoutConfig.coop_credit_amount;
        
        return Component.extend({
            defaults: {
                template: 'Mynamespace_Coop/checkout/shipping/coopcredit'
            },
            canVisibleCoopBlock: show_hide_Coop_blockConfig,
            getFormattedPrice: ko.observable(priceUtils.formatPrice(coop_credit_amount, quote.getPriceFormat())),
            getCoopcreditLabel:ko.observable(coopcredit_label)
        });
    });
