/**
 * @api
 */
define([
    'Magento_Customer/js/model/customer',
    'Magento_Checkout/js/model/url-builder',
    'mageUtils'
], function (customer, urlBuilder, utils) {
        'use strict';

        return {

            /**
             * @param {String} coopCredit
             * @param {String} quoteId
             * @return {*}
             */
            getApplyCoopUrl: function (coopCredit, quoteId) {
                var params = this.getCheckoutMethod() == 'guest' ? //eslint-disable-line eqeqeq
                        {
                            quoteId: quoteId
                        } : {},
                    urls = {
                        //'guest': '/guest-carts/' + quoteId + '/coupons/' + encodeURIComponent(coopCredit),
                        'customer': '/carts/mine/coop/' + encodeURIComponent(coopCredit)
                    };

                return this.getUrl(urls, params);
            },
            /**
             * Get url for service.
             *
             * @param {*} urls
             * @param {*} urlParams
             * @return {String|*}
             */
            getUrl: function (urls, urlParams) {
                var url;

                if (utils.isEmpty(urls)) {
                    return 'Provided service call does not exist.';
                }

                if (!utils.isEmpty(urls['default'])) {
                    url = urls['default'];
                } else {
                    url = urls[this.getCheckoutMethod()];
                }

                return urlBuilder.createUrl(url, urlParams);
            },

            /**
             * @return {String}
             */
            getCheckoutMethod: function () {
                return customer.isLoggedIn() ? 'customer' : 'guest';
            }
        };
    }
);
