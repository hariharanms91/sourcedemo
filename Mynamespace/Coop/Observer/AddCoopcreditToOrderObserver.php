<?php
namespace Mynamespace\Coop\Observer;

use Magento\Framework\Event\Observer as EventObserver;
use Magento\Framework\Event\ObserverInterface;

class AddCoopcreditToOrderObserver implements ObserverInterface
{
    /**
     * Set payment coopcredit to order
     *
     * @param EventObserver $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $quote = $observer->getQuote();
        $CoopCredit = $quote->getCoopcredit();
        if (!$CoopCredit) {
            return $this;
        }
        //Set coopcredit data to order
        $order = $observer->getOrder();
        $order->setData('coopcredit', $CoopCredit);
        
		return $this;
    }
}
