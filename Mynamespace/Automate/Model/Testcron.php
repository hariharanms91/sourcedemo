<?php
namespace Mynamespace\Automate\Model;

use \Psr\Log\LoggerInterface;

class Testcron
{
    /**
     *  @var \Psr\Log\LoggerInterface
     */
    protected $_logger;

    /**
     * @param LoggerInterface $logger
     */
    public function __construct(
        LoggerInterface $logger
    ) {
        $this->_logger = $logger;
    }

    /**
     *
     * @return $this
     */
    public function LogMessage()
    {
        $this->_logger->info("Testing My logMessage");
        return $this;
    }
}